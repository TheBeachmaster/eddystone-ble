let http = require('http');
let eddystone  = require('eddystone-beacon');

let url =  "https://git.io";

let opt = {
    name: "AT Beacon",
    txPowerLevel:-22,
    tlmCount:2,
    tlmPeriod:10
};

eddystone.advertiseUrl(url, opt);

http.createServer( (req, res) => {
    console.log("Beacon Up");
}).listen(6000);



